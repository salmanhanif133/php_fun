<?php
    //1. create connection
    $dbhost = "localhost";
    $dbuser = "widget_cms";
    $dbpass = "salmanhanif33";
    $dbname = "widget_corp";
    //Test if connection succeeded
    $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    if(mysqli_connect_errno()) {
        die("Database connection failed: " . 
        mysqli_connect_error() . " (". mysqli_connect_errno() .")");
    }
?>

<?php
// perfom query
    $query = "SELECT * FROM subjects";
    $result = mysqli_query($connection,$query);
    if(!$result) {
        die('Database query failed'); 
    }
?>
<!doctype html>

<html lang="en">
    <head>
        <title>Databases</title>
        <meta charset="utf-8">
    </head>

    <body>
        
    <ul>
    <?php
    //use return data
        while($row = mysqli_fetch_assoc($result)) {
         echo "<li>" . $row['menu_name'] . " (" . $row['id'] . ")</li>"; 
        }
        //release
        mysqli_free_result($result);
    ?>
    </ul>

    </body>
</html>
<?php

//close
    mysqli_close($connection);
?>